module gitlab.com/mcje_/repository_pattern

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.4
	github.com/golang/mock v1.6.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.16
)
