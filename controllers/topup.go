package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/mcje_/repository_pattern/database"
	"gitlab.com/mcje_/repository_pattern/models"
	"gitlab.com/mcje_/repository_pattern/repositories"
)

type TopupControllers interface {
	IndexTopup(w http.ResponseWriter, r *http.Request)
	CreateTopUp(w http.ResponseWriter, r *http.Request)
}

type topupController struct {
	topupRepo repositories.TopupRepo
}

func NewTopupController(t repositories.TopupRepo) TopupControllers {
	return topupController{
		topupRepo: t,
	}
}

func (t topupController) IndexTopup(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	topup, err := t.topupRepo.IndexTopup()

	if err != nil {
		panic(err)
	}

	json.NewEncoder(w).Encode(topup)
}

func (t topupController) CreateTopUp(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	var topup models.Topup
	walletRepo := repositories.NewWalletRepo(database.DB)
	err := json.NewDecoder(r.Body).Decode(&topup)

	if err != nil {
		panic(err)
	}
	tx := database.DB.Begin()

	_, err = t.topupRepo.CreateTopup(topup, tx)

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	err = walletRepo.TopUPWallet(topup.IDWallet, topup.Total, tx)

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()

	json.NewEncoder(w).Encode(topup)
}
