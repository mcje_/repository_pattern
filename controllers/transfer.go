package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/mcje_/repository_pattern/database"
	"gitlab.com/mcje_/repository_pattern/models"
	"gitlab.com/mcje_/repository_pattern/repositories"
)

type TransferControllers interface {
	IndexTransfer(w http.ResponseWriter, r *http.Request)
	CreateTransfer(w http.ResponseWriter, r *http.Request)
}

type transferController struct {
	transferRepo repositories.TransferRepo
}

func NewTransferController(tr repositories.TransferRepo) TransferControllers {
	return transferController{
		transferRepo: tr,
	}
}

func (tr transferController) IndexTransfer(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	transfer, err := tr.transferRepo.IndexTransfer()

	if err != nil {
		panic(err)
	}

	json.NewEncoder(w).Encode(transfer)
}

func (tr transferController) CreateTransfer(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	var transfer models.Transfer
	walletRepo := repositories.NewWalletRepo(database.DB)
	err := json.NewDecoder(r.Body).Decode(&transfer)

	if err != nil {
		panic(err)
	}

	tx := database.DB.Begin()
	_, err = tr.transferRepo.CreateTransfer(transfer, tx)

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	walletSender, err := walletRepo.FindUserFirstWallet(transfer.IDSender)

	if err != nil {
		panic(err)
	}

	walletSender.Total = walletSender.Total - transfer.Total
	_, err = walletRepo.UpdateWallet(walletSender, tx)

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	walletReceiver, err := walletRepo.FindUserFirstWallet(transfer.IDReceiver)

	if err != nil {
		panic(err)
	}

	walletReceiver.Total = walletReceiver.Total + transfer.Total
	_, err = walletRepo.UpdateWallet(walletReceiver, tx)

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	tx.Commit()
}
