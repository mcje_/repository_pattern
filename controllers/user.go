package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mcje_/repository_pattern/database"
	"gitlab.com/mcje_/repository_pattern/models"
	"gitlab.com/mcje_/repository_pattern/repositories"
)

type UserControllers interface {
	GetAllUser(w http.ResponseWriter, r *http.Request)
	FindUser(w http.ResponseWriter, r *http.Request)
	CreateOrUpdateUser(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
	// Transfer(w http.ResponseWriter, r *http.Request)
}

type userController struct {
	userRepo repositories.UserRepo
}

func NewUserController(u repositories.UserRepo) UserControllers {
	return userController{
		userRepo: u,
	}
}

func (u userController) GetAllUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	users := u.userRepo.IndexUsers()
	json.NewEncoder(w).Encode(users)
}

func (u userController) FindUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	users := u.userRepo.ShowUser(chi.URLParam(r, "id_user"))
	json.NewEncoder(w).Encode(users)
}

func (u userController) CreateOrUpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	var user models.User
	var walletRepo = repositories.NewWalletRepo(database.DB)

	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		panic(err)
	}

	var res models.User

	if user.ID == 0 {
		tx := database.DB.Begin()
		res, err = u.userRepo.CreateUser(user, tx)

		if err != nil {
			tx.Rollback()
			panic(err)
		}

		_, err := walletRepo.CreateWallet(models.Wallet{
			Name:   "My Wallet",
			IDUser: res.ID,
			Total:  0,
		}, tx)

		if err != nil {
			tx.Rollback()
			panic(err)
		}

		tx.Commit()
	} else {
		res, err = u.userRepo.UpdateUser(user, nil)
	}

	if err != nil {
		panic(err)
	}

	json.NewEncoder(w).Encode(res)
}

func (u userController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	err := u.userRepo.DeleteUser(chi.URLParam(r, "id_user"))
	if err != nil {
		panic(err)
	}
}
