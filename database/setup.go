package database

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var DB *gorm.DB

func Connect() {
	dsn := "root:@tcp(127.0.0.1:3306)/e-wallet?charset=utf8mb4&parseTime=True&loc=Local"
	config := gorm.Config{Logger: logger.Default.LogMode(logger.Info)}
	db, err := gorm.Open(mysql.Open(dsn), &config)

	if err != nil {
		panic("can't connect to database!")
	}

	DB = db
}
