package routes

import (
	"gitlab.com/mcje_/repository_pattern/database"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mcje_/repository_pattern/controllers"
	"gitlab.com/mcje_/repository_pattern/repositories"
)

func UserRoutes(r *chi.Mux) {
	var userRepo = repositories.NewUserRepo(database.DB)
	var userController = controllers.NewUserController(userRepo)
	r.Route("/user", func(r chi.Router) {
		r.Get("/", userController.GetAllUser)
		r.Get("/{id_user}", userController.FindUser)
		r.Post("/", userController.CreateOrUpdateUser)
		r.Delete("/{id_user}", userController.DeleteUser)
	})
}
