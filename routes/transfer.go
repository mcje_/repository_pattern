package routes

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/mcje_/repository_pattern/controllers"
	"gitlab.com/mcje_/repository_pattern/database"
	"gitlab.com/mcje_/repository_pattern/repositories"
)

func TransferRoutes(r *chi.Mux) {
	transferRepo := repositories.NewTransferRepo(database.DB)
	transferController := controllers.NewTransferController(transferRepo)
	r.Route("/transfer", func(r chi.Router) {
		r.Get("/", transferController.IndexTransfer)
		r.Post("/", transferController.CreateTransfer)
	})
}
