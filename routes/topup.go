package routes

import (
	"github.com/go-chi/chi/v5"
	"gitlab.com/mcje_/repository_pattern/controllers"
	"gitlab.com/mcje_/repository_pattern/database"
	"gitlab.com/mcje_/repository_pattern/repositories"
)

func TopUpRoutes(r *chi.Mux) {
	var topUpRepo = repositories.NewTopupRepo(database.DB)
	var topUpController = controllers.NewTopupController(topUpRepo)
	r.Route("/topup", func(r chi.Router) {
		r.Get("/", topUpController.IndexTopup)
		r.Post("/", topUpController.CreateTopUp)
	})
}
