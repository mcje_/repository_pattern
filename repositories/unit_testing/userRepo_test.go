package unit_testing

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"gitlab.com/mcje_/repository_pattern/models"
	"gitlab.com/mcje_/repository_pattern/repositories/mocks"
)

func Test_CreateUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	expectedID := 1
	expectedName := "Michael"
	expectedAge := 19
	expectedAddress := "Bandung"

	expectedUser := models.User{
		ID:      uint(expectedID),
		Name:    expectedName,
		Age:     uint(expectedAge),
		Address: expectedAddress,
	}

	mockUserRepo := mocks.NewMockUserRepo(ctrl)
	mockUserRepo.EXPECT().CreateUser(expectedUser, nil).Return(expectedUser, nil).Times(1)

	u, err := mockUserRepo.CreateUser(expectedUser, nil)
	require.NoError(t, err)

	require.Equal(t, expectedUser.ID, u.ID)
	require.Equal(t, expectedUser.Name, u.Name)
	require.Equal(t, expectedUser.Age, u.Age)
	require.Equal(t, expectedUser.Address, u.Address)
}

func Test_ShowUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	expectedUser := models.User{
		ID:      1,
		Name:    "Michael",
		Age:     19,
		Address: "Bandung",
	}

	mockUserRepo := mocks.NewMockUserRepo(ctrl)
	mockUserRepo.EXPECT().ShowUser(expectedUser.ID).Return(expectedUser).Times(1)

	u := mockUserRepo.ShowUser(expectedUser.ID)

	require.Equal(t, expectedUser.ID, u.ID)
	require.Equal(t, expectedUser.Name, u.Name)
	require.Equal(t, expectedUser.Age, u.Age)
	require.Equal(t, expectedUser.Address, u.Address)
}

func Test_UpdateUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	user := models.User{
		ID:      1,
		Name:    "Michael",
		Age:     19,
		Address: "Bandung",
	}

	user.Name = "Michael Jeremia"

	expectedUser := models.User{
		ID:      1,
		Name:    "Michael Jeremia",
		Age:     19,
		Address: "Bandung",
	}

	mockUserRepo := mocks.NewMockUserRepo(ctrl)
	mockUserRepo.EXPECT().UpdateUser(user, nil).Return(expectedUser, nil).Times(1)

	u, err := mockUserRepo.UpdateUser(user, nil)
	require.NoError(t, err)

	require.Equal(t, user.ID, u.ID)
	require.Equal(t, user.Name, u.Name)
	require.Equal(t, user.Age, u.Age)
	require.Equal(t, user.Address, u.Address)
}

func Test_IndexUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	expectedUsers := []models.User{
		models.User{
			ID:      1,
			Name:    "Michael",
			Age:     19,
			Address: "Bandung",
		},
		models.User{
			ID:      2,
			Name:    "Jere",
			Age:     19,
			Address: "Bandung",
		},
	}

	mockUserRepo := mocks.NewMockUserRepo(ctrl)
	mockUserRepo.EXPECT().IndexUsers().Return(expectedUsers).Times(1)

	u := mockUserRepo.IndexUsers()

	require.Equal(t, u, expectedUsers)
}

func Test_DeleteUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	users := []models.User{
		models.User{
			ID:      1,
			Name:    "Michael",
			Age:     19,
			Address: "Bandung",
		},
	}

	mockUserRepo := mocks.NewMockUserRepo(ctrl)
	mockUserRepo.EXPECT().DeleteUser(users[0].ID).Return(nil).Times(1)

	err := mockUserRepo.DeleteUser(users[0].ID)

	require.NoError(t, err)
}
