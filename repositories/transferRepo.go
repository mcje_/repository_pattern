package repositories

import (
	"gitlab.com/mcje_/repository_pattern/models"
	"gorm.io/gorm"
)

type TransferRepo interface {
	IndexTransfer() ([]models.Transfer, error)
	CreateTransfer(t models.Transfer, tx *gorm.DB) (models.Transfer, error)
}

type transferRepo struct {
	DB *gorm.DB
}

func NewTransferRepo(db *gorm.DB) TransferRepo {
	return transferRepo{
		DB: db,
	}
}

func (tr transferRepo) IndexTransfer() ([]models.Transfer, error) {
	var err error
	var transfer []models.Transfer
	err = tr.DB.Find(&transfer).Error
	return transfer, err
}

func (tr transferRepo) CreateTransfer(t models.Transfer, tx *gorm.DB) (models.Transfer, error) {
	var err error
	var db *gorm.DB
	if tx != nil {
		db = tx
	} else {
		db = tr.DB
	}
	err = db.Create(&t).Error
	return t, err
}
