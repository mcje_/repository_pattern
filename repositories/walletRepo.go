package repositories

import (
	"gitlab.com/mcje_/repository_pattern/models"
	"gorm.io/gorm"
)

type WalletRepo interface {
	CreateWallet(wallet models.Wallet, tx *gorm.DB) (models.Wallet, error)
	TopUPWallet(idWallet uint, amount uint, tx *gorm.DB) error
	FindUserFirstWallet(id_user uint) (models.Wallet, error)
	UpdateWallet(wallet models.Wallet, tx *gorm.DB) (models.Wallet, error)
}

type walletRepo struct {
	DB *gorm.DB
}

func NewWalletRepo(db *gorm.DB) WalletRepo {
	return walletRepo{
		DB: db,
	}
}

func (w walletRepo) CreateWallet(wallet models.Wallet, tx *gorm.DB) (models.Wallet, error) {
	var err error
	var db *gorm.DB
	if tx != nil {
		db = tx
	} else {
		db = w.DB
	}
	err = db.Create(&wallet).Error
	return wallet, err
}

func (w walletRepo) UpdateWallet(wallet models.Wallet, tx *gorm.DB) (models.Wallet, error) {
	var err error
	var db *gorm.DB
	var mapWallet = make(map[string]interface{})
	if tx != nil {
		db = tx
	} else {
		db = w.DB
	}
	mapWallet["name"] = wallet.Name
	mapWallet["total"] = wallet.Total
	err = db.Model(&wallet).Updates(&mapWallet).Error
	return wallet, err
}

func (w walletRepo) TopUPWallet(idWallet uint, amount uint, tx *gorm.DB) error {
	var err error
	var db *gorm.DB
	if tx != nil {
		db = tx
	} else {
		db = w.DB
	}
	err = db.Table("wallets").Where("id = ?", idWallet).Update("total", gorm.Expr("total + ?", amount)).Error
	return err
}

func (w walletRepo) FindUserFirstWallet(id_user uint) (models.Wallet, error) {
	var err error
	var wallet models.Wallet
	err = w.DB.Where("id_user = ?", id_user).Order("created_at asc").First(&wallet).Error

	if err != nil {
		panic(err)
	}

	return wallet, err
}
