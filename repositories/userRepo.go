package repositories

import (
	"gitlab.com/mcje_/repository_pattern/models"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

//go:generate mockgen -source=userRepo.go -destination mocks/mock_userRepo.go -package mocks UserRepo

type UserRepo interface {
	IndexUsers() []models.User
	ShowUser(id interface{}) models.User
	CreateUser(user models.User, tx *gorm.DB) (models.User, error)
	UpdateUser(user models.User, tx *gorm.DB) (models.User, error)
	DeleteUser(id interface{}) error
}

type userRepo struct {
	DB *gorm.DB
}

func NewUserRepo(db *gorm.DB) UserRepo {
	return userRepo{
		DB: db,
	}
}

func (u userRepo) IndexUsers() []models.User {
	var users []models.User
	err := u.DB.Preload("Wallets").Find(&users)

	if err.Error != nil {
		panic(err.Error)
	}

	return users
}

func (u userRepo) ShowUser(id interface{}) models.User {
	var user models.User
	err := u.DB.Preload("Wallets").Where("id = ?", id).First(&user)

	if err.Error != nil {
		panic(err.Error)
	}

	return user
}

func (u userRepo) CreateUser(user models.User, tx *gorm.DB) (models.User, error) {
	var err error
	var db *gorm.DB
	if tx != nil {
		db = tx
	} else {
		db = u.DB
	}
	err = db.Omit(clause.Associations).Create(&user).Error
	return user, err
}

func (u userRepo) UpdateUser(user models.User, tx *gorm.DB) (models.User, error) {
	var err error
	var db *gorm.DB
	if tx != nil {
		db = tx
	} else {
		db = u.DB
	}
	err = db.Omit(clause.Associations).Model(&user).Updates(&user).Error
	return user, err
}

func (u userRepo) DeleteUser(id interface{}) error {
	err := u.DB.Where("id = ?", id).Delete(models.User{}).Error
	return err
}
