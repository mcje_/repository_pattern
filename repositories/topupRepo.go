package repositories

import (
	"gitlab.com/mcje_/repository_pattern/models"
	"gorm.io/gorm"
)

type TopupRepo interface {
	IndexTopup() ([]models.Topup, error)
	CreateTopup(topup models.Topup, tx *gorm.DB) (models.Topup, error)
	// UpdateTopup(topup models.TopUp, tx *gorm.DB) (models.TopUp, error)
}

type topupRepo struct {
	DB *gorm.DB
}

func NewTopupRepo(db *gorm.DB) TopupRepo {
	return topupRepo{
		DB: db,
	}
}

func (t topupRepo) IndexTopup() ([]models.Topup, error) {
	var err error
	var topup []models.Topup

	err = t.DB.Find(&topup).Error
	return topup, err 
}

func (t topupRepo) CreateTopup(topup models.Topup, tx *gorm.DB) (models.Topup, error) {
	var err error
	var db *gorm.DB
	if tx != nil {
		db = tx
	} else {
		db = t.DB
	}
	err = db.Create(&topup).Error
	return topup, err
}

// func (t topupRepo) UpdateTopup(topup models.TopUp, tx *gorm.DB) (models.TopUp, error) {
// 	var err error
// 	var db *gorm.DB
// 	if tx != nil {
// 		db = tx
// 	} else {
// 		db = t.DB
// 	}
// 	err = db.Updates(&topup).Error
// 	return topup, err
// }
