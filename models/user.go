package models

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	ID        uint           `gorm:"primaryKey;autoIncrement:true" json:"id"`
	Name      string         `json:"name"`
	Age       uint           `json:"age"`
	Address   string         `json:"address"`
	Wallets   []Wallet       `json:"wallets" gorm:"foreignKey:IDUser"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
}
