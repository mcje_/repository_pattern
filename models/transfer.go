package models

import (
	"time"

	"gorm.io/gorm"
)

type Transfer struct {
	ID         uint           `gorm:"primaryKey;autoIncrement:true" json:"id"`
	IDSender   uint           `json:"id_sender"`
	IDReceiver uint           `json:"id_receiver"`
	Total      uint           `json:"total"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	DeletedAt  gorm.DeletedAt `json:"deleted_at"`
}
