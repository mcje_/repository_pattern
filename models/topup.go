package models

import (
	"time"

	"gorm.io/gorm"
)

type Topup struct {
	ID        uint           `gorm:"primaryKey;autoIncrement:true" json:"id"`
	IDUser    uint           `json:"id_user"`
	IDWallet  uint           `json:"id_wallet"`
	Total     uint           `json:"total"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
}
