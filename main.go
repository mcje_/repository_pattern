package main

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/mcje_/repository_pattern/database"
	"gitlab.com/mcje_/repository_pattern/routes"
)

func main() {
	database.Connect()

	r := chi.NewRouter()

	routes.UserRoutes(r)
	routes.TopUpRoutes(r)
	routes.TransferRoutes(r)

	http.ListenAndServe(":8060", r)
}
